﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using AppCEP.Servicos;
using AppCEP.Servicos.Modelos;

namespace AppCEP
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            Botao.Clicked += Botao_Clicked;
        }

        private void Botao_Clicked(object sender, EventArgs e)
        {
            string cep = CEP.Text;
            
            if (IsValidCEP(cep))
            {
                try
                {
                    Endereco end = ViaCEPServico.BuscarEnredecoViaCEP(cep);

                    if(end != null)
                    {
                        Resultado.Text = $"Endereço: {end.Logradouro}, {end.Bairro}, {end.Localidade}, {end.UF}.";
                    }
                    else
                    {
                        DisplayAlert("Erro", "Não foi possível encontrar o endereço deste CEP", "OK");
                    }
                }
                catch(Exception ex)
                {
                    DisplayAlert("Erro", "Ocorreu um erro:" + ex.Message, "OK");
                }
            }

            //throw new NotImplementedException();
        }

        private bool IsValidCEP(string cep)
        {
            bool Valid = true;
            if (cep.Length != 8)
            {
                DisplayAlert("Atenção","O CEP deve possuir 8 números","Ok");
                Valid = false;
            }
            int novoCEP = 0;
            if (!int.TryParse(cep, out novoCEP))
            {
                DisplayAlert("Atenção", "O CEP deve possuir apenas números", "Ok");
                Valid = false;
            }

            return Valid;
        }
    }
}
