﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using AppCEP.Servicos.Modelos;
using Newtonsoft.Json;

namespace AppCEP.Servicos
{
    class ViaCEPServico
    {
        private static string EnderecoURL = "https://viacep.com.br/ws/{0}/json/";

        public static Endereco BuscarEnredecoViaCEP(string cep)
        {
            string NovoEnderecoURL = string.Format(EnderecoURL, cep);

            WebClient wc = new WebClient();
            string Conteudo = wc.DownloadString(NovoEnderecoURL); // Esse método que acessa o site e faz o download em string.

            Endereco end = JsonConvert.DeserializeObject<Endereco>(Conteudo);

            if (end == null) return null;

            return end;
        }
    }
}
